from pymongo import MongoClient
import os

MONGO_DIR = os.path.expandvars('${HOME}/pyjournal/data/mongo_db.conf')
MONGO_HOST = open(MONGO_DIR,'r').read()

def makeDBConn():
    client = MongoClient(host = MONGO_HOST)
    return client.journal

def dbinsert(db,json):
    db.st_aquinas.insert_one(json)
