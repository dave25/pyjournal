from jsonRetriever import *
from datetime import datetime
import math

months = ["n/a","January", "February", "March", "April",
          "May", "June", "July", "August",
          "September", "October", "November", "December"]
days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
class DayLog:
    def __init__(self,initLog=0):
        self.logList = []
        self.showFixedData = True
        self.showTags = True
        self.fixedData = {}
        if(initLog != 0):
            self.date = datetime.fromtimestamp(initLog.timestamp)
            self.tsday = (self.date.day,self.date.month,self.date.year)
            self.logList.append(initLog)
            self.fixedData = initLog.fixedData

    def printDay(self):
        self.printHeader()
        print("")
        self.printLogs()

    def printHeader(self):
        year = self.date.year
        month = months[self.date.month]
        day = self.date.day
        week_day = days[self.date.weekday()]
        print("------------------------------------------------------------------------------------------")
        print("\t\t\t\t/*******{0} {1} {2}, {3}*******/".format(week_day,month,day,year))
        print("------------------------------------------------------------------------------------------")
        if(self.showFixedData):
            for k,v in self.fixedData.items():
                print("|<{0}:{1}>".format(k,v))

    def printLogs(self):
        for l in self.logList:
            print("")
            self._printSingleLog(l)

    def setDate(self,date):
        self.date = date

    def addLog(self,l):
        if l.getTSDay() != self.tsday:
            return False
        else:
            self.logList.append(l)
            self.fixedData.update(l.fixedData)
            return True
    def size(self):
        return len(self.logList)

    def _populateFixedData(self):
        for l in self.logList:
            self.fixedData.update(l.fixedData)

    def _printSingleLog(self,l):
        time = datetime.fromtimestamp(l.timestamp)
        hour, minute = time.hour,time.minute
        print(">{0}:{1}\n {2}".format(hour,minute,l.content.rstrip()))
        if(self.showTags):
            print(l.tags)
