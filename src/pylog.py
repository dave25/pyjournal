#!/usr/bin/python3
from log import Log
import sys, os, tempfile
from subprocess import call
from pymongo import MongoClient
from mongoConn import *
from datetime import datetime
import sys
import jsonTool
import logView

DATA_DIR = os.path.expandvars('${HOME}/pyjournal/data/journal.json')
#Format input functions

def fireEdit():
    contentFile = tempfile.NamedTemporaryFile(mode='r+',suffix=".tmp",encoding="utf-8")
    EDITOR = os.environ.get("EDITOR","vim")
    contentFile.write('#Type your log!. Save and quit when finished.\n'
                      '#Lines starting with # will not be saved on your log!\n')
    contentFile.flush()
    call([EDITOR,contentFile.name])
    contentFile.seek(0)
    textlst = []
    textlst = map(lambda e: ''.join(e),contentFile)
    nocomment = lambda e:not e[0] == '#'
    textlst = filter(nocomment, textlst)
    content = ''.join(textlst)
    return content


def command(com):
    if com == "help":
        _help()
    elif com == "log":
        _log()
    elif com == "view":
        logView.printAllLogs()
    else:
        print("Command not found!")

def _log(setTag=True,setFixed=True,setTS=True):
    global DATA_DIR
    objLog = Log()
    inpt = fireEdit()
    objLog.setLog(inpt)
    if setTag:
        print("Tags: Comma-separated. Enter when finished")
        inpt = input("tags> ")
        objLog.setTag(inpt)
    if setFixed:
        print("Fixed data: Introduce comma-separated pairs of key-values, colon separated, e.g. Location:US, X:Y, ...")
        inpt = input("fixedData> ")
        objLog.setFixedData(inpt)
    if setTS:
        while True:
            try:
                print("Enter log date in the following formar hour[:min[:day[:month[:year]]]] Blank parameters defaults to current datetime")
                inp = input("timestamp> ")
                now = datetime.now()
                lnow = [now.hour, now.minute, now.day, now.month, now.year]
                dt = inp.split(":")
                for i in range(len(dt)):
                    lnow[i] = dt[i]
                print("The log will be saved with the following date. Are you sure [y/n]")
                print(dict(hour=lnow[0],minute=lnow[1],day=lnow[2],month=lnow[3],year=lnow[4]))
                choice = input()
                while (choice != 'y' and choice != 'yes' and choice != 'n' and choice != 'no'):
                    choice = input("Type yes or no> ")
                if(choice == 'y' or choice == 'yes'):
                    timestmp = datetime(hour=int(lnow[0]),minute=int(lnow[1]),day=int(lnow[2]),month=int(lnow[3]),year=int(lnow[4])).timestamp()
                elif(choice == 'n' or choice == 'no'):
                    continue
                break
            except TypeError as err:
                print("Bad datetime syntax: {0} Try again".format(err))
        objLog.setTimestamp(timestmp)
    else:
        objLog.setTimestamp()
    jsonObj = jsonTool.buildJson(objLog)
    try:
        db = makeDBConn();
        dbinsert(db,jsonTool.buildJsonDict(jsonObj))
    except:
        print("An error ocurred while saving your log in the Database. Saving log in local file")
        saveFile = open(DATA_DIR,'a')
        saveFile.write(jsonObj)
        saveFile.write("\n")
    print(jsonObj)

def _help():
    print("Help typed")


def interactive():
    print("Welcome to pyLog!")
    print("Type 'help' to view command list")
    stin = input('> ')
    while stin != "exit":
        if stin != '':
            command(stin)
        stin = input("> ")
    print("Bye!")

if len(sys.argv) > 1:
    sh_arg = ''.join(list(filter(lambda e: e[0]=='-', sys.argv)))
    lng_arg = ''.join(list(filter(lambda e: e[0]=='--', sys.argv)))
    setTag = False
    setFixed = False
    setTS = False
    if 't' in sh_arg or '--set-tag' in lng_arg:
        setTag = True
    if 'x' in sh_arg or '--set-fixed-data' in lng_arg:
        setFixed = True
    if 'd' in sh_arg or '--set-datetime' in lng_arg:
        setTS = True
    if "l" in sh_arg or "--log" in lng_arg:
        _log(setTag,setFixed,setTS)
    if "v" in sh_arg or "--view" in lng_arg:
        logView.printAllLogs()

else:
    interactive()


