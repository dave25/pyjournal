import json

def buildJson(obj):
    return json.dumps(obj,cls=objEncode)

def buildJsonDict(jsonObj):
    return json.loads(jsonObj)

class objEncode(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj,object):
            return obj.__dict__
        return json.JSONEncoder.default(self,obj)
