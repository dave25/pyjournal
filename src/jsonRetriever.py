from datetime import datetime
from time import time
from mongoConn import makeDBConn
from log import Log
import pymongo
import math

def getSingleDateLogs(day=-1,month=-1,year=-1):
    if year == -1:
        year = datetime.fromtimestamp(time()).year
    if month == -1:
        month = datetime.fromtimestamp(time()).month
    if day == -1:
        day = datetime.fromtimestamp(time()).day
    t1 = datetime(year,month,day).timestamp()
    t2 = datetime(year,month,day,23,59).timestamp()
    query = {"timestamp" : {"$gt" : t1} , "timestamp" : {"$lt" : t2}}
    db = makeDBConn()
    return db.st_aquinas.find(query)

def getAllLogs():
    logs =  makeDBConn().st_aquinas.find().sort([("timestamp",pymongo.ASCENDING)])
    response = []
    for l in logs:
        response.append(Log(l['content'],
                            l['timestamp'],
                            l['tags'],
                            l['fixedData']))
    return response
