import time
import datetime

def formatTag(inpt):
    lst = inpt.split(',')
    ls = list(map(lambda e: e.lstrip().rstrip(),lst))
    nows = lambda e: not(e == '\n' or e == '\t' or e == '' or ' ' in e or '\t' in e)
    return list(filter(nows,ls))

def formatFixed(inpt):
    res = {}
    splt = inpt.split(',')
    temp = list(map(lambda e: e.lstrip().rstrip(),splt))
    nows = lambda e: not(e == '\n' or e == '\t' or e == '' or ' ' in e or '\t' in e)
    pairs = list(filter(nows,temp))
    if len(pairs) == 0:
        return {}
    if len(pairs) == 0:
        return {}
    for pair in pairs:
        t = pair.split(':')
        res[t[0]] = t[1]
    return res

class Log:
    def __init__(self, content = '', timestamp = 0,tags = [],fixedData ={}):
        self.content = content
        self.timestamp = timestamp
        self.tags = tags
        self.fixedData = fixedData

    def setLog(self, sysinpt):
        content = ''.join(sysinpt)
        self.content = content

    def setTag(self, sysinpt):
        tags = formatTag(sysinpt)
        self.tags = tags

    def setFixedData(self,sysinpt):
        fixedData = formatFixed(sysinpt)
        self.fixedData = fixedData

    def setTimestamp(self, ts = time.time()):
        self.timestamp =ts
    def getTSDay(self):
        date = datetime.datetime.fromtimestamp(self.timestamp)
        return (date.day,date.month,date.year)
