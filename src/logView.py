from dayLogView import DayLog
import jsonRetriever

def printAllLogs():
    logs = jsonRetriever.getAllLogs()
    daylog = DayLog()
    listdays = []
    for l in logs:
        if daylog.size() == 0:
            daylog = DayLog(l)
        elif not daylog.addLog(l):
            listdays.append(daylog)
            daylog = DayLog(l)
    listdays.append(daylog) #append last day not included in for-loop
    for l in listdays:
        l.printDay()


